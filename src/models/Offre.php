<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 07/02/2019
 * Time: 14:05
 */

namespace ccd\models;


class Offre extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'offre';
    protected $primaryKey = '[id, user, categorie]' ;
    public $timestamps = true ;
}