<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 07/02/2019
 * Time: 12:21
 */

namespace ccd\models;

class User extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'user';
    protected $primaryKey = 'id' ;
    public $timestamps = true ;
}