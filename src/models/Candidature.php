<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 07/02/2019
 * Time: 13:33
 */

namespace ccd\models;


class Candidature extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'candidature';
    protected $primaryKey = '[id, user, offre]' ;
    public $timestamps = true ;
}