<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 07/02/2019
 * Time: 12:23
 */

namespace ccd\models;


class Categorie extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'categorie';
    protected $primaryKey = 'id' ;
    public $timestamps = false ;
}