<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 07/02/19
 * Time: 10:07
 */
session_start();
require_once '../vendor/autoload.php';
use Illuminate\Database\Capsule\Manager as DB;
use ccd\controllers as c;
use ccd\structure as s;
use ccd\views as v;
$app = new \Slim\Slim();
echo s\Head::getHead();
echo "<body>";
echo s\Nav::getNav();
$db = new DB();
$db->addConnection(parse_ini_file("../conf/conf.ini"));
$db->setAsGlobal();
$db->bootEloquent();

$app->get('/', function (){

    echo (new c\ControlAccueil())->displayHome();
});

$app->get('/sign', function(){
    echo (new c\ControlInscription())->displayFormulaire();
});

$app->post('/validatesign', function (){
    v\Inscription::ValidationInscr($_POST['email']);
});

$app->get('/login', function (){
    echo (new c\ControlConnexion())->displayConnexion();
});

$app->get('/disconnect', function (){
    (new c\ControlDeconnexion())->displayDeconnexion();
});

$app->get('/myaccount', function (){
    echo (new c\ControlMyAccount())->displayMyAccount();
});

$app->get('/accountModify', function (){
    echo (new c\ControlModifyMyAccount())->displayModifMyAccount();
});

$app->post('/myaccount/validate', function (){
    (new c\ControlValidateModifAccount())->displayValidateMyAccount();
});

$app->post('/validatelog', function (){
    v\Connexion::ValidationConn($_POST['email'], $_POST['password']);
});

$app->get('/listUser', function (){
    echo (new c\ControlListUser())->displayList();
});

$app->get('/offres',function (){
    echo(new c\ControlOffres())->displayOffres();
});

$app->get('/myoffers',function (){
    echo(new c\ControlMesOffres())->displayMyOffres();
});

$app->get('/createoffer',function (){
    echo(new c\ControlCreateOffer())->displayCreateOffres();
});

$app->post('/validatecreateoffer',function (){
    (new c\ControlValidateCreateOffer())->displayValidateCreateOffer();
});

$app->get('/mycandidature',function (){
    echo(new c\ControlMyCandidature())->displayMyCandidature();
});

$app->get('/postuler', function(){
    $app = \Slim\Slim::getInstance();
    $idoffre= $app->request()->get('id');
   echo v\Offres::postuler($idoffre);
});

$app->post('/validpost', function(){
    $app = \Slim\Slim::getInstance();
    $idoffre= $app->request()->get('id');
    echo v\Offres::validerCandidature($idoffre);
});

$app->run();

echo s\Footer::getFooter();


