<?php
/**
 * Created by PhpStorm.
 * User: leo
 * Date: 07/02/19
 * Time: 21:34
 */

namespace ccd\controllers;

use ccd\views\Offres as Offres;

class ControlValidateCreateOffer
{
    public function displayValidateCreateOffer(){
        (new Offres())->ValidateCreateOffer();
    }
}