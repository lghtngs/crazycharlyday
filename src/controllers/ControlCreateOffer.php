<?php
/**
 * Created by PhpStorm.
 * User: leo
 * Date: 07/02/19
 * Time: 21:31
 */

namespace ccd\controllers;

use ccd\views\Offres as Offres;

class ControlCreateOffer
{
    public function displayCreateOffres(){
        return (new Offres())->CreerOffre();
    }
}