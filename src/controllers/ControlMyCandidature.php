<?php
/**
 * Created by PhpStorm.
 * User: leo
 * Date: 07/02/19
 * Time: 22:07
 */

namespace ccd\controllers;

use ccd\views\Offres as Offres;

class ControlMyCandidature
{
    public function displayMyCandidature(){
        return (new Offres())->myCandidature();
    }
}