<?php
/**
 * Created by PhpStorm.
 * User: MSI-Sofiane
 * Date: 07/02/2019
 * Time: 11:36
 */
namespace ccd\controllers;
use ccd\views\Apropos as Apropos;

class ControlApropos
{
    public function displayCarousel(){
        echo (new Apropos())->render();
    }
}