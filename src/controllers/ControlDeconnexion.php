<?php
/**
 * Created by PhpStorm.
 * User: leo
 * Date: 07/02/19
 * Time: 16:43
 */

namespace ccd\controllers;
use ccd\views\Connexion as Conn;

class ControlDeconnexion
{
    public function displayDeconnexion(){
        (new Conn())->Disconnect();
    }
}