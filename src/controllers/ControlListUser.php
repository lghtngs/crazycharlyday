<?php
/**
 * Created by PhpStorm.
 * User: MSI-Sofiane
 * Date: 07/02/2019
 * Time: 16:02
 */

namespace ccd\controllers;
use ccd\views\ListUser as ListUser;

class ControlListUser
{

    public function displayList(){
        return (new ListUser())->render();
    }

    public function displayUser(){

    }
}