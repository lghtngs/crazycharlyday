<?php
namespace ccd\controllers;
use ccd\views\Offres as Offres;

class ControlOffres

{
    public function displayOffres(){
        return (new Offres())->afficherOffres();
    }
}