<?php
/**
 * Created by PhpStorm.
 * User: leo
 * Date: 07/02/19
 * Time: 21:23
 */

namespace ccd\controllers;

use ccd\views\Offres as Offres;

class ControlMesOffres
{
    public function displayMyOffres(){
        return (new Offres())->myOffres();
    }
}