<?php
/**
 * Created by PhpStorm.
 * User: MSI-Sofiane
 * Date: 07/02/2019
 * Time: 14:40
 */

namespace ccd\controllers;
use ccd\views\Accueil as Accueil;

class ControlAccueil
{
    public function displayHome(){
        return (new Accueil())->AfficherAccueil();
    }
}