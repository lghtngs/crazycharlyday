<?php
/**
 * Created by PhpStorm.
 * User: leo
 * Date: 07/02/19
 * Time: 18:27
 */

namespace ccd\controllers;

use ccd\views\EspacePersonnel;

class ControlModifyMyAccount
{
    public function displayModifMyAccount(){
        return (new EspacePersonnel())-> ModifierInformations();
    }
}