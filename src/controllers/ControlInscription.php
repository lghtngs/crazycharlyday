<?php
/**
 * Created by PhpStorm.
 * User: MSI-Sofiane
 * Date: 07/02/2019
 * Time: 15:21
 */

namespace ccd\controllers;
use ccd\views\Inscription as Inscription;

class ControlInscription
{
    public function displayFormulaire(){
        return (new Inscription())->Formulaire();
    }
}