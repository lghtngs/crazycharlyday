<?php

namespace ccd\controllers;
use ccd\views\Connexion as Conn;

class ControlConnexion
{
    public function displayConnexion(){
        return (new Conn())->Login();
    }
}