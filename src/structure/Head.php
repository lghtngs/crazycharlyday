<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 10/12/2018
 * Time: 11:13
 */

namespace ccd\structure;


/**
 * Class Head gérant le code html du Head
 * @package wishlist\Structure
 */
class Head
{

    /**
     * affiche le head
     * @param string $title
     * @param String $css
     * @return string html
     */

    public static function getHead()
    {

        $html = '
        <!DOCTYPE html>
    <html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>JustJobs</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" >
        <link rel="icon" type="image/png" href="images/parse-resume.png" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
        <link rel="stylesheet" href="web/css/style.css">
        <script src="web/js/script.js"></script>
    </head>

         ';

        return $html;
    }
}