<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 10/12/2018
 * Time: 13:33
 */

namespace ccd\structure;


/**
 * Class Footer gérant le code html du footer
 * @package wishlist\Structure
 */
class Footer {

    /**
     * affichager du footer
     * @return string html
     */

    public static function getFooter()
    {

        $html = '
        <footer class="text-center">
            <a href="#about">
                <svg xmlns="http://www.w3.org/2000/svg" width="10" height="16" viewBox="0 0 10 16"><path fill-rule="evenodd" d="M10 10l-1.5 1.5L5 7.75 1.5 11.5 0 10l5-5 5 5z"/></svg>
            </a>
            <h5>© TOASTEAM, TOUS DROITS RÉSERVÉS.</h5>
        </footer>
    </body>
</html>';
        return $html;
    }
}