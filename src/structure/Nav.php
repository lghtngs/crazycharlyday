<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 18/12/2018
 * Time: 10:46
 */

namespace ccd\structure;
use Slim\Slim;

/**
 * Class Nav gérant le code html du menu
 * @package wishlist\Structure
 */
class Nav {

    /**
     * affiche le menu
     * @return string
     */
    public static function getNav()
    {
        $app = Slim::getInstance();
        $html = '<nav class="navbar navbar-expand-md navbar-dark sticky-top">
        <a class="navbar-brand" href="#"></a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#myNavbar">
            <i class="fas fa-bars fa-lg"></i>
        </button>

        <div class="collapse navbar-collapse justify-content-center" id="myNavbar">
        ';
        if (!isset($_SESSION['email'])) {
            $html = $html . '<ul class="nav nav-pills navbar-nav">
                <li class="nav-item"><a class="nav-link" href="/">Accueil</a></li>
                <li class="nav-item"><a class="nav-link" href="/offres">Offres d\'emplois</a></li>
                <li class="nav-item"><a class="nav-link" href="/listUser">Liste d\'utilisateurs</a></li>
                <li class="nav-item"><a class="nav-link" href="/sign">Inscription</a></li>
                <li class="nav-item"><a class="nav-link" href="/login">Connexion</a></li> 
                </ul>
              </div>
         </nav>
        ';
        } else {
                $html = $html . '<ul class="nav nav-pills navbar-nav">
                <li class="nav-item"><a class="nav-link" href="/">Accueil</a></li>
                <li class="nav-item"><a class="nav-link" href="/listUser">Liste d\'utilisateurs</a></li>
                <li class="nav-item"><a class="nav-link" href="/offres">Offres d\'emplois</a></li>
                <li class="nav-item"><a class="nav-link" href="/myoffers">Mes offres d\'emplois</a></li>
                <li class="nav-item"><a class="nav-link" href="/mycandidature">Mes candidatures</a></li>
                <li class="nav-item"><a class="nav-link" href="/disconnect">Deconnexion</a></li>
                <li class="nav-item"><a class="nav-link" href="/myaccount">Espace personnel</a></li>
                </ul>
              </div>
         </nav>
        ';

        }
        return $html;
    }

}