<?php
/**
 * Created by PhpStorm.
 * User: leo
 * Date: 07/02/19
 * Time: 13:32
 */

namespace ccd\views;

use ccd\models\AppType;
use ccd\models\User;

/**
 * Class Connexion gérant les méthodes de connexion
 * @package ccd\views
 */
class Connexion
{
    /**
     * affiche le formulaire de connexion
     */
    public static function Login()
    {
        $form = "
            <div class=\"formation-block\">
                <h3>Formulaire de connexion</h3>
                <form action='/validatelog' method='POST'>
                    <span>Adresse mail: </span><input type='email' name= 'email' placeholder='E-Mail' required><br>
                    <span>Mot de passe: </span><input type='password' name = 'password' required><br>
                    <div class='bt'><input type='submit' value='Valider'></div>
                </form>
            </div>";
        return $form;
    }

    /**
     * verifie si le couple identifiant/mdp sont conforme à la base de données avec
     * l'identifiant et le mdp entrée en paramètre
     * @param $em String
     * @param $pass String
     */
    public static function ValidationConn($em, $pass)
    {
        $check = User::select('id')->where('email', '=', $em)->first();
        if (isset($check) && $check->id > 0) {
            $passsql = User::select('mdp')->where('email', '=', $em)->first();
            $pass = password_verify($pass, $passsql->mdp);
            if ($pass) {
                $connected = User::select('id','nom', 'prenom')->where('email', '=', $em)->first();
                $_SESSION['nom'] = $connected->nom;
                $_SESSION['prenom'] = $connected->prenom;
                $_SESSION['email'] = $em;
                header("Location: index.php");
                die();

            } else {
                echo "<p class='erreur'>L'addresse ou le mot de passe est invalide</p>";
                self::Login();

            }
            unset($temp);
        } else {
            echo "<p class='erreur'>Un problème s'est produit, veuillez réessayer. Si le problème persiste, contactez l'adminitrateur du site</p>";
            self::Login();
        }
    }

    /**
     * se deconnecter
     */
    public static function Disconnect()
    {
        session_unset();
        header("Location: index.php");
        die();
    }

}


