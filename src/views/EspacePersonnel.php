<?php
/**
 * Created by PhpStorm.
 * User: leo
 * Date: 07/02/19
 * Time: 18:15
 */

namespace ccd\views;


class EspacePersonnel
{

    /**
     * affiche l'espace personnel
     * @return string html
     */
    public static function AfficherEspacePersonnel()
    {

        $user =  \ccd\models\User::where('email', '=', $_SESSION['email'])->first();
        $html = '
            <div class="formation-block">
            <h2>Information sur le compte</h2>
                <div class="info_id">
                    <h3>Informations identifiants</h3>
                    <p><strong>E-mail:</strong> ' . $user->email . '</p>
                    <p><strong>Mot de passe:</strong> <i>-- caché --</i></p>
                </div>
                <div class="info_general">
                    <h3>Informations Générales</h3>
                    <p><strong>Nom :</strong> ' . $user->nom . '</p>
                    <p><strong>Prénom :</strong> ' . $user->prenom . '</p>
                    <p><strong>Date de naissance :</strong> ' . $user->dateNaiss . '</p>
                    <p><strong>lieu :</strong> ' . $user->lieu . '</p>
                </div>
                <div class="button1"><a href="/accountModify">Modifier mes informations</a></div>
            </div>
           
        ';

        return $html;

    }

    /**
     * affiche un formulaire de modification des informations personnels
     * @return string html
     */
    public static function ModifierInformations()
    {
        $user = \ccd\models\User::where('email', '=', $_SESSION['email'])->first();
        $html = '
        
            <div class="formation-block">
            <form action="../myaccount/validate" method ="post">
                <h2>Information sur le compte</h2>
                <div class="info_compte">
                    <div class="info_id">
                        <h3>Informations identifiants</h3>
                        <p><strong>E-mail:</strong> <input name="mail" type="email" required value="' . $user->email . '"></p>
                        <p><strong>Ancien mot de passe:</strong> <input name="old_pass" type="password"></p>
                        <p><strong>Nouveau mot de passe:</strong> <input name="new_pass" type="password" minlength="8"></p>
                        <p><strong>Confirmer mot de passe:</strong> <input name="confirm_pass" type="password" minlength="8"></p>
                    </div>
                    <div class="info_general">
                        <h3>Informations générales</h3>
                        <p><strong>Nom:</strong> <input name="new_nom" type="text" required value="' . $user->nom . '"></p>
                        <p><strong>Prénom:</strong> <input name="new_prenom" type="text" required value="' . $user->prenom . '"></p>
                        <p><strong>lieu:</strong> <input name="new_lieu" type="text" required value="' . $user->lieu . '"></p>
                    </div>
                </div>
                <div class="bt"><input type="submit" value="Enregistrer mes nouvelles informations"></div>
                </form>
            </div>  
        ';

        return $html;
    }

    /**
     * sauvegarde les modifications de l'utilisateur
     */
    public static function ValiderModifications()
    {
        $user = \ccd\models\User::where('email', '=', $_SESSION['email'])->first();
        if (!empty($_POST['old_pass'])) {
            if (password_verify($_POST['old_pass'], $user->password)) {
                if ($_POST['new_pass'] === $_POST['confirm_pass']) {
                    $user->password = password_hash($_POST['new_pass'], PASSWORD_DEFAULT);
                }
            }
        }
        $user->lieu = filter_var($_POST['new_lieu'], FILTER_SANITIZE_SPECIAL_CHARS);
        $user->email = filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL);
        $user->nom = filter_var($_POST['new_nom'], FILTER_SANITIZE_SPECIAL_CHARS);
        $user->prenom = filter_var($_POST['new_prenom'], FILTER_SANITIZE_SPECIAL_CHARS);
        $user->save();
        header("Location: ../../myaccount");
        die();
    }
}

