<?php
/**
 * Created by PhpStorm.
 * User: MSI-Sofiane
 * Date: 07/02/2019
 * Time: 11:30
 */
namespace ccd\views;

class Apropos
{
    /**
     * affichage la page a propos
     * @return string html
     */

    public static function render()
    {

        $html = '
        <section id="apropos">
            <div class="container">
                <div class="red-divider"></div>
                <div class="heading">
                    <h2>Divers</h2>
                </div>
                <div id="myCarousel" class="carousel slide text-center" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <h3>Nous la Toasteam</h3>
                            <h4>Amateur de Toasty !</h4>
                        </div>
                        <div class="carousel-item">
                            <h3>On est au Crazy</h3>
                            <h4>Charly Day de folie !</h4>
                        </div>
                        <div class="carousel-item">
                            <h3>14 heures intensives</h3>
                            <h4>Pour un travail créatif !</h4>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#myCarousel" data-slide="prev" role="button">
                        <svg xmlns="http://www.w3.org/2000/svg" width="8" height="16" viewBox="0 0 8 16"><path fill-rule="evenodd" d="M5.5 3L7 4.5 3.25 8 7 11.5 5.5 13l-5-5 5-5z"/></svg>
                    </a>
                    <a class="carousel-control-next" href="#myCarousel" data-slide="next" role="button">
                        <svg xmlns="http://www.w3.org/2000/svg" width="8" height="16" viewBox="0 0 8 16"><path fill-rule="evenodd" d="M7.5 8l-5 5L1 11.5 4.75 8 1 4.5 2.5 3l5 5z"/></svg>
                    </a>
                </div>
            </div>
        </section>
  ';
        return $html;
    }
}