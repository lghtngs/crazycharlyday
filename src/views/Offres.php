<?php
namespace ccd\views;

use ccd\models\Candidature;
use ccd\models\Offre;
use ccd\models\User;

class Offres
{

    public function afficherOffres()
    {

        $html ='<section id="formation">
            <div class="container">
                
                <div class="row">
                    ';
        $postuler = '';
        $offres = Offre::all();
        foreach ($offres as $lign) {
            $html .= $this->ajouterOffre($lign);
        }
        $html.='
                </div>
            </div>     
        </section>';

        return $html;
    }

    public function ajouterOffre($offre){
        $html = '<div class="col-sm-4">
                        <div class="formation-block">
                            <h5>Offre</h5>
                            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="16" viewBox="0 0 12 16"><path fill-rule="evenodd" d="M12 14.002a.998.998 0 0 1-.998.998H1.001A1 1 0 0 1 0 13.999V13c0-2.633 4-4 4-4s.229-.409 0-1c-.841-.62-.944-1.59-1-4 .173-2.413 1.867-3 3-3s2.827.586 3 3c-.056 2.41-.159 3.38-1 4-.229.59 0 1 0 1s4 1.367 4 4v1.002z"/></svg>
                            <h3>';
        $html .= $offre->nom;
        $html.='</h3>
                            <h4> Dernière mise à jour le :';
        $html.=$offre->updated_at;
        $html.='</h4>
                            <div class="red-divider"></div>
                            <p>';
        $html.=$offre->description;
        $html.='</p>';
        if(isset($_SESSION['email'])){
            $html.= '<a class="button1"  href = /postuler?id=' . $offre->id . '>Postuler</a></li>';
        }

          $html.=' </div>
                    </div>';

        return $html;
    }

    public static function postuler($id)
    {
        $offre = Offre::where('id', '=', $id)->first();
        $html = '';
        $html = $html . "
        <div class=\"formation-block\">
            <h3>Postuler pour $offre->nom</h3>
            <form action='/validpost?id=$id' method ='post' enctype=\"multipart/form-data\">
                <span>Adresse actuelle</span><input type='text' name='adract' ><br>
                <input type='hidden' name='MAX_FILE_SIZE' value='10000000'>
                <span>CV<input type='file' name ='cvup' accept= '.pdf'></span><br>
                <input type='hidden' name='MAX_FILE_SIZE' value='10000000'>
                <span>Lettre de motivation<input type='file' name ='lettremotiv' accept= '.pdf'></span><br>
             
                
                <input type='submit' value='Postuler'>
               
             </form>
        </div>
        ";

        return $html;
    }

    public static function validerCandidature($id)
    {

        $user= User::where('email','=',$_SESSION['email'])->first();
        $candidature= new Candidature();
        $candidature->user = $user->id;
        $candidature->offre= $id;


        $folder = 'pdf/';
        echo $folder;
        $file = basename($_FILES['cvup']['name']);
        if (move_uploaded_file($_FILES['cvup']['tmp_name'], $folder . uniqid(). $file)) {
            echo "Uploaded";
            $user->cv = $file;
        } else {
            echo "Failed !";
        }
        $file2 = basename($_FILES['lettremotiv']['name']);
        if (move_uploaded_file($_FILES['lettremotiv']['tmp_name'], $folder . uniqid().$file2)) {
            echo "Uploaded";
            $candidature->lettremotivation = $file2;
        } else {
            echo "Failed !";
        }
            $user->save();
            $candidature->save();
            header("Location: index.php");
            die();
        }


    public static function myOffres(){
        $html = '<a class="nav-link bouton2" href="/createoffer">Creer une offre</a>';
        $html = $html . "<ul>
                    <div class=\"formation-block\">";
        $offres = Offre::all();
        $user= User::where('email','=',$_SESSION['email'])->first();

        foreach ($offres as $lign) {
            if($lign->user === $user->id) {
                $html = $html . '<li>' . $lign->nom . ' ' . $lign->description . ' Dernière mise à jour le :' . $lign->updated_at . '<a href = /postuler?id=' . $lign->id . '>Postuler</a></li>';
            }
        }
        $html = $html . "</div></ul>";
        return $html;
    }


    public static function CreerOffre(){
        $html = '';
        $html = $html . "
        <div class='formation-block'>
                <h3>Formulaire d'inscription</h3>
                <form action='/validatecreateoffer' method='POST'>
                    <span>Nom de l'offre </span><input type='text' name ='nom' placeholder='Nom'  required><br>
                    <span>Description : </span><input type='text' name = 'description' placeholder='Description' required><br>
                    <span>Lieu :</span><input type='text' name= 'lieu' placeholder='Lieu' required><br>
                    <div class='bt'><input type='submit' value='Valider'></div>
                </form>
            </div>";
        return $html;
    }

    /**
     * sauvegarde les modifications de l'utilisateur
     */
    public static function ValidateCreateOffer()
    {
        $offre = new Offre();
        $user= User::where('email','=',$_SESSION['email'])->first();
        $offre->user = $user->id;
        $offre->nom = filter_var($_POST['nom'], FILTER_SANITIZE_SPECIAL_CHARS);
        $offre->description = filter_var($_POST['description'], FILTER_SANITIZE_SPECIAL_CHARS);
        $offre->lieu = filter_var($_POST['lieu'], FILTER_SANITIZE_SPECIAL_CHARS);
        $offre->save();
        header("Location: ../myoffers");
        die();
    }


    public static function myCandidature(){
        $html = "<ul>
                    <div class=\"formation-block\">";
        $candidature = Candidature::all();
        $user= User::where('email','=',$_SESSION['email'])->first();
        foreach ($candidature as $lign) {
            if($lign->user === $user->id) {
                $offre= Offre::where('id','=', $lign->offre)->first();
                $html = $html . '<li>' . $offre->nom.' '.$offre->description. '</li>';
            }
        }
        $html = $html . "</div></ul>";
        return $html;
    }


}