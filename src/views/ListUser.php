<?php
/**
 * Created by PhpStorm.
 * User: MSI-Sofiane
 * Date: 07/02/2019
 * Time: 16:03
 */

namespace ccd\views;

use ccd\models\Type;
use ccd\models\AppType;
use ccd\models\User;

class ListUser
{

    /**
     * affiche tous les utilisateurs
     * @return string
     */
    public function render(){
        $user = User::all();
        $html = '<section id="about" class="container-fluid">
            <div class="col-xs-8 col-md-4 profile-picture">
                <img src="../img/user.png" alt="Sofiane" class="rounded-circle">
            </div>
            <div class="heading">
                <h1>Liste des utilisateurs</h1>
            </div>
        </section>';
        $html .= '<section id="formation">
            <div class="container">
                
                <div class="row">
                    ';
        foreach ($user as $u){
            $html .= $this->addUser($u);


        }
        $html.='
                </div>
            </div>     
        </section>';
        return $html;
    }

    /**
     * ajoute un utilisateur
     * @param $user
     * @return string
     */
    public function addUser($user){
        $html ='<div class="col-sm-4">
                        <div class="formation-block">
                            <h5>Utilisateur ';
        $html .=$user->id;
        $html.='</h5>
                            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="16" viewBox="0 0 12 16"><path fill-rule="evenodd" d="M12 14.002a.998.998 0 0 1-.998.998H1.001A1 1 0 0 1 0 13.999V13c0-2.633 4-4 4-4s.229-.409 0-1c-.841-.62-.944-1.59-1-4 .173-2.413 1.867-3 3-3s2.827.586 3 3c-.056 2.41-.159 3.38-1 4-.229.59 0 1 0 1s4 1.367 4 4v1.002z"/></svg>
                            <h3>';
        $html .= $user->nom .' '.$user->prenom;
        $html.='</h3> <h4>';
        $html .= $user->dateNaiss;
        $html .= '</h4>
         <div class="red-divider"></div>
                            <p>';
        $html .=$user->email;
        $html .='</p>
                        </div>
                    </div>';
        return $html;
    }
}