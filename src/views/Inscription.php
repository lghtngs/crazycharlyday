<?php
/**
 * Created by PhpStorm.
 * User: leo
 * Date: 07/02/19
 * Time: 12:51
 */

namespace ccd\views;

/**
 * Class Inscription gérant l'inscription
 * @package ccd\views
 */

class Inscription
{

    /**
     * affiche un formulaire d'inscription
     */
    public static function Formulaire()
    {

        $form = "
             <div class=\"formation-block\">
                <h3>Formulaire d'inscription</h3>
                <form action='/validatesign' method='POST'>
                    <span>Nom : </span><input type='text' name ='nom' placeholder='Nom'  required><br>
                    <span>Prenom : </span><input type='text' name = 'prenom' placeholder='Prénom' required><br>
                    <span>Adresse mail :</span><input type='email' name= 'email' placeholder='E-Mail' required><br>
                    <span>date de naissance : </span><input type='date' name = 'dateNaiss' placeholder='Code postale' required><br>
                    <span>Adresse :</span><input type='text' name = 'adresse' placeholder='Adresse' required><br>
                    <span>Mot de passe : </span><input type='password' name = 'password' minlength='8'  required><br>
                    <span>Confirmer mot de passe : </span><input type='password' name = 'confirm' minlength='8'  required><br>
                    <div class='bt'><input type='submit' value='Valider'></div>
                </form>
            </div>";

        return $form;
    }

    /**
     * insere un nouvelle utilisateur dans la base de données
     * @param $em String
     */
    public static function ValidationInscr($em)
    {
        $em = filter_var($em, FILTER_SANITIZE_EMAIL);
        $mail = \ccd\models\User::select('id')->where('email', '=', $em)->first();
        if (isset($mail)) {
            echo "<p class='erreur'>L'adresse e-mail est déjà associée à un compte</p>";
            self::Formulaire();
        } elseif ($_POST['password'] !== $_POST['confirm']) {
            echo "<p class='erreur'>Les deux mots de passes ne correspondent pas</p>";
            self::Formulaire();
        } else {
            $user = new \ccd\models\User();
            $user->nom = filter_var($_POST['nom'], FILTER_SANITIZE_SPECIAL_CHARS);
            $user->prenom = filter_var($_POST['prenom'], FILTER_SANITIZE_SPECIAL_CHARS);
            $user->email = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);
            $user->dateNaiss = $_POST['dateNaiss'];
            $user->lieu = filter_var($_POST['adresse'], FILTER_SANITIZE_SPECIAL_CHARS);
            $user->mdp = password_hash($_POST['password'], PASSWORD_DEFAULT);
            $user->save();
            $_SESSION['prenom'] = $user->prenom;
            $_SESSION['nom'] = $user->nom;
            $_SESSION['user_id'] = $user->user_id;
            $_SESSION['email'] = $user->email;
            $_SESSION['cp'] = $user->codePostal;

            unset($_POST);
            header("Location: index.php");
            die();
        }

    }
}