<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 07/02/19
 * Time: 13:31
 */

namespace ccd\views;

class Accueil{

    /**
     * affiche l'accueil
     * @return string
     */
public static function AfficherAccueil(){
    $html= '   <section id="apropos">
            <div class="container">
                <div class="red-divider"></div>
                <div class="heading">
                    <h2>Bienvenue sur JustJobs !</h2>
                </div>
                <div id="myCarousel" class="carousel slide text-center" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <h2>La référence dans la recherche d\'emploi</h2>
                            <h3>Pour les personnes de tout handicap</h3>
                        </div>
                        <div class="carousel-item">
                            <img src="../img/2.jpg" alt="Second Slide">
                        </div>
                        <div class="carousel-item">
                            <img src="../img/3.jpg" alt="Third Slide">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#myCarousel" data-slide="prev" role="button">
                        <svg xmlns="http://www.w3.org/2000/svg" width="8" height="16" viewBox="0 0 8 16"><path fill-rule="evenodd" d="M5.5 3L7 4.5 3.25 8 7 11.5 5.5 13l-5-5 5-5z"/></svg>
                    </a>
                    <a class="carousel-control-next" href="#myCarousel" data-slide="next" role="button">
                        <svg xmlns="http://www.w3.org/2000/svg" width="8" height="16" viewBox="0 0 8 16"><path fill-rule="evenodd" d="M7.5 8l-5 5L1 11.5 4.75 8 1 4.5 2.5 3l5 5z"/></svg>
                    </a>
                </div>
            </div>
        </section>
        
        ';


    $html.='<section id="experience">
            <div class="container">
                <div class="white-divider">

                </div>
                <div class="heading">
                    <h2>Expertise Professionelle</h2>
                </div>
                <ul class="timeline">


                    <li>
                        <div class="timeline-badge"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 14 16"><path fill-rule="evenodd" d="M9 4V3c0-.55-.45-1-1-1H6c-.55 0-1 .45-1 1v1H1c-.55 0-1 .45-1 1v8c0 .55.45 1 1 1h12c.55 0 1-.45 1-1V5c0-.55-.45-1-1-1H9zM6 3h2v1H6V3zm7 6H8v1H6V9H1V5h1v3h10V5h1v4z"/></svg></div>
                        <div class="timeline-panel-container">
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h3>Étape 1</h3>
                                    <h4>Inscription</h4>
                                    <p class="text-muted"><svg xmlns="http://www.w3.org/2000/svg" width="12" height="16" viewBox="0 0 12 16"><path fill-rule="evenodd" d="M6 8h2v1H5V5h1v3zm6 0c0 2.22-1.2 4.16-3 5.19V15c0 .55-.45 1-1 1H4c-.55 0-1-.45-1-1v-1.81C1.2 12.16 0 10.22 0 8s1.2-4.16 3-5.19V1c0-.55.45-1 1-1h4c.55 0 1 .45 1 1v1.81c1.8 1.03 3 2.97 3 5.19zm-1 0c0-2.77-2.23-5-5-5S1 5.23 1 8s2.23 5 5 5 5-2.23 5-5z"/></svg>  Moins de 5 min</p>
                                </div>
                                <div class="timeline-body">
                                    <p>Inscription à notre site</p>
                                    <p>Accès à votre espace personnel</p>
                                    <p>Modifier vos informations</p>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="timeline-badge"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 14 16"><path fill-rule="evenodd" d="M9 4V3c0-.55-.45-1-1-1H6c-.55 0-1 .45-1 1v1H1c-.55 0-1 .45-1 1v8c0 .55.45 1 1 1h12c.55 0 1-.45 1-1V5c0-.55-.45-1-1-1H9zM6 3h2v1H6V3zm7 6H8v1H6V9H1V5h1v3h10V5h1v4z"/></svg></div>
                        <div class="timeline-panel-container-inverted">
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h3>Étape 2</h3>
                                <h4>Rechercher une offre</h4>
                                <p class="text-muted"><svg xmlns="http://www.w3.org/2000/svg" width="12" height="16" viewBox="0 0 12 16"><path fill-rule="evenodd" d="M6 8h2v1H5V5h1v3zm6 0c0 2.22-1.2 4.16-3 5.19V15c0 .55-.45 1-1 1H4c-.55 0-1-.45-1-1v-1.81C1.2 12.16 0 10.22 0 8s1.2-4.16 3-5.19V1c0-.55.45-1 1-1h4c.55 0 1 .45 1 1v1.81c1.8 1.03 3 2.97 3 5.19zm-1 0c0-2.77-2.23-5-5-5S1 5.23 1 8s2.23 5 5 5 5-2.23 5-5z"/></svg>  De 10 à 30 min</p>
                            </div>
                            <div class="timeline-body">
                                <p>Accès à une multitudes d\'offres</p>
                                <p>Nos clients sont nombreux</p>
                                <p>Offres variées et adaptées</p>
                            </div>
                        </div>
                        </div>
                    </li>


                    <li>
                        <div class="timeline-badge"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 14 16"><path fill-rule="evenodd" d="M9 4V3c0-.55-.45-1-1-1H6c-.55 0-1 .45-1 1v1H1c-.55 0-1 .45-1 1v8c0 .55.45 1 1 1h12c.55 0 1-.45 1-1V5c0-.55-.45-1-1-1H9zM6 3h2v1H6V3zm7 6H8v1H6V9H1V5h1v3h10V5h1v4z"/></svg></div>
                        <div class="timeline-panel-container">
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h3>Étape 3</h3>
                                    <h4>Postuler</h4>
                                    <p class="text-muted"><svg xmlns="http://www.w3.org/2000/svg" width="12" height="16" viewBox="0 0 12 16"><path fill-rule="evenodd" d="M6 8h2v1H5V5h1v3zm6 0c0 2.22-1.2 4.16-3 5.19V15c0 .55-.45 1-1 1H4c-.55 0-1-.45-1-1v-1.81C1.2 12.16 0 10.22 0 8s1.2-4.16 3-5.19V1c0-.55.45-1 1-1h4c.55 0 1 .45 1 1v1.81c1.8 1.03 3 2.97 3 5.19zm-1 0c0-2.77-2.23-5-5-5S1 5.23 1 8s2.23 5 5 5 5-2.23 5-5z"/></svg>  Moins de 3 min</p>
                                </div>
                                <div class="timeline-body">
                                    <p>Envoie de votre CV et lettre de motivation</p>
                                    <p>En espérant une réponse positive !</p>
                                </div>
                            </div>
                        </div>
                    </li>

                </ul>
            </div>
        </section>';
    $html.='<section id="portfolio">
            <div class="container">
                <div class="white-divider"></div>
                <div class="heading">
                    <h2>Nous vous mettons en relation avec des chauffeurs équipés...</h2>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <a class="thumbnail" target="_blank">
                            <img src="../img/eh1.jpg" alt="facebook share">
                        </a>
                    </div>
                    <div class="col-sm-4">
                        <a class="thumbnail" target="_blank">
                            <img src="../img/eh2.jpg" alt="Piccadily Game">
                        </a>
                    </div>
                    <div class="col-sm-4">
                        <a class="thumbnail" target="_blank">
                            <img src="../img/eh3.jpg" alt="UBBA">
                        </a>
                    </div>
                </div>
            </div>
            </section>';
    return $html;
}
}